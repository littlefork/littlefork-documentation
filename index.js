import * as _ from 'lodash';
import {pluginNames, copyPluginReadmeFiles, copyFile, createDirectories, createFile, htmlList, pluginNamesHumanReadable, htmlLinks, version} from './lib/utils';

// General directories for markdown files
export const contentDir = './content';
export const pluginsDir = '/plugins';
export const latestDir = '/latest';
export const pathToPlugins = `${contentDir}${pluginsDir}`;

// Templates
const templatesDir = 'layouts';
const menuTemplateFile = 'menu.pug';

// Developers guide readme
const developersGuideSrc = './node_modules/littlefork/docs/developers-guide.md';
const developersGuideDest = `${contentDir}/developers-guide/index.md`;

// Tutorial readme
const tutorialSrc = './node_modules/littlefork/docs/tutorial.md';
const tutorialDest = `${contentDir}/tutorial/index.md`;

// Toolkit readme
const toolkitSrc = './node_modules/littlefork-toolkit/README.md';
const toolkitDest = `${contentDir}/${latestDir}/index.md`;

// Littlefork API readme
const apiSrc = './node_modules/littlefork/docs/api.md';
const apiDest = `${contentDir}/api/index.md`;

// Documentation readme
const readmeSrc = 'README.md';
const readmeDest = `${contentDir}/index.md`;


// Make sure directories exist before copying readme files
createDirectories();

// Copying files
console.log('Copying documentation files from plugins.');
copyPluginReadmeFiles(pluginNames());

console.log('Copying developers-guide.');
copyFile(developersGuideSrc, developersGuideDest);

console.log('Copying tutorial.');
copyFile(tutorialSrc, tutorialDest);

console.log('Copying toolkit documentation.');
copyFile(toolkitSrc, toolkitDest);

console.log('Copying API documentation.');
copyFile(apiSrc, apiDest);

console.log('Copying this applications Readme file.');
copyFile(readmeSrc, readmeDest);


// Building menu template with links to all the index.html files
const pluginsNames = pluginNames();
const pluginsPaths = _.map(pluginsNames, n => `/plugins/${n}`);
const pluginsLinks = htmlLinks(pluginsPaths, pluginNamesHumanReadable(pluginsNames));
const toolkitLink = htmlLinks(['/latest'], ['Toolkit']);
const developersGuideLink = htmlLinks(['/developers-guide/'], ['Developers guide']);
const tutorialLink = htmlLinks(['/tutorial/'], ['Tutorial']);
const apiLink = htmlLinks(['/api/'], ['API']);
const restApiLink = htmlLinks(['/api/rest'], ['Rest API']);

console.log(`Building menu template ${templatesDir}/${menuTemplateFile}...`);
createFile(`${templatesDir}/${menuTemplateFile}`,
  `<p class="version">Version: ${version()}</p><nav>`
  .concat('<span>Getting started</span>')
  .concat(htmlList(tutorialLink))
  .concat('<span>API documentation</span>')
  .concat(htmlList(_.concat(apiLink, restApiLink)))
  .concat('<span>Core and plugin documentation</span>')
  .concat(htmlList(_.concat(toolkitLink, pluginsLinks)))
  .concat('<span>For developers</span>')
  .concat(htmlList(_.concat(developersGuideLink)))
  .concat('</nav>')
);
