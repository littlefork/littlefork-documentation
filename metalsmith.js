const Metalsmith = require('metalsmith');
const markdown = require('metalsmith-markdown');
const layouts = require('metalsmith-layouts');
const pandoc = require('metalsmith-pandoc');
const path = require('metalsmith-path');
const sass = require('metalsmith-sass');
const sc = require('metalsmith-static');

console.log('Building metalsmith static site...');
Metalsmith(__dirname)
    .source('./content')
    .destination('./_site')
    .use(markdown())
    .use(path())
    .use(sc({
      src: 'assets',
      dest: 'assets'
    }))
    .use(sass({
      outputStyle: 'expanded',
      outputDir: 'assets/css/'
    }))
    .use(pandoc({
      pattern: '**/*.md',
      from: 'markdown',
      to: 'html5'
    }))
    .use(layouts({
      engine: 'pug',
      default: 'main.pug',
      rename: true
    }))
    .build(function (err) { if(err) console.log(err) });
