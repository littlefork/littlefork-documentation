import * as _ from 'lodash';
import fs from 'fs-extra';

import {contentDir, pathToPlugins, latestDir} from '../index';

const pathToToolkitConfig = './node_modules/littlefork-toolkit/package.json';
const pluginNamingConvention = 'littlefork'; // the starting characters indicating littlefork plugins like 'littlefork-plugin-mongodb'

// Setup the directories necessary for this application
export const createDirectories = () => {
  // ensure content dir exists
  fs.mkdirsSync(contentDir);
  // ensure plugins dir exists
  fs.mkdirsSync(pathToPlugins);
  // ensure dir for latest toolkit version exists
  const latest = `${contentDir}${latestDir}`;
  fs.mkdirsSync(latest);
};

export const createFile = (fileName, data) => {
  fs.writeFile(fileName, data, (err) => {
    if (err) throw err;
    console.log(`Success creating file ${fileName}!`);
  });
};

export const copyFile = (src, dest) => {
  console.log(`Trying to copy ${src}...`);
  try {
    fs.copySync(src, dest);
  } catch (err) {
    console.log("Error copying file!");
    return;
  }
  console.log("Done copying file!");
};

/**
 * Version number
 * @returns {String} This applications version number
 */
export const version = () => {
  const obj = JSON.parse(fs.readFileSync('package.json', 'utf8'));
  return obj.version;
};

/**
 * Littlefork plugin names included the littlefork-toolkit package.
 * @returns {Array} Plugin names.
 */
export const pluginNames = () => {
  let pluginNames = [];
  const obj = JSON.parse(fs.readFileSync(pathToToolkitConfig, 'utf8'));
  _.forEach(obj.dependencies, (v, k) => {
    if (_.startsWith(k, pluginNamingConvention)) {
      pluginNames = _.concat(pluginNames, k);
    }
  });
  return pluginNames;
};

/**
 * Littlefork plugin names included the littlefork-toolkit package in a formatted format.
 * @param {Array} List with plugin names.
 * @returns {Array} Plugin names included the littlefork-toolkit package.
 */
export const pluginNamesHumanReadable = (pluginNames) =>
  _.map(pluginNames, n =>
    _.capitalize(_.replace(n, new RegExp('-', 'g'), ' '))
  );

/* Copy Readme files from littlefork packages included in the littlefork-toolkit
 * into the content folder.
 * README.md files are renamed to Readme.md.
*/
export const copyPluginReadmeFiles = (pluginNamesArray) => {
  _.forEach(pluginNamesArray, p => {
    // create plugin dir
    fs.mkdirsSync(`${pathToPlugins}/${p}`);
    try {
      // try copying Readme.md file
      console.log(`Trying to copy ./node_modules/${p}/Readme.md...`);
      fs.copySync(`./node_modules/${p}/Readme.md`, `${pathToPlugins}/${p}/index.md`);
    } catch (err) {
      if (err.code === 'ENOENT') {
        try {
          // .. if there was no Readme.md file, try copying README.md file
          console.log(`Readme.md not found for this plugin, Trying to copy ./node_modules/${p}/README.md...`);
          fs.copySync(`./node_modules/${p}/README.md`, `${pathToPlugins}/${p}/index.md`);
        } catch (errr) {
          console.log(`Error copying Readme.md or README.md file from toolkit plugin ${p}: ${errr.code}. Maybe the file does not exist?`);
        }
      }
    }
  });
  console.log('Done copying files!');
};

/**
 * Turns an array into a HTML list
 * @param {Array} Elements which should be surrounded by <li> elements
 * @returns {String} HTML list
 */
export const htmlList = (array) =>
  `<ul>${_.join(_.map(array, elem =>
    `<li>${elem}</li>`
  ), '')}</ul>`;

  /**
   * Produces HTML links
   * @param {Array} Paths which should be linked to.
   * @param {Array} Labels which should be used as the link text
   * @returns {String} Html list
   */
export const htmlLinks = (links, labels) =>
  _.map(_.zip(links, labels), p =>
    `<a href="${p[0]}">${p[1]}</a>`
  );


export default {
  pluginNames,
  pluginNamesHumanReadable,
  copyPluginReadmeFiles,
  copyFile,
  createDirectories,
  version,
  htmlList,
  htmlLinks
};
