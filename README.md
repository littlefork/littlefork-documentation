
# Littlefork documentation

Aggregating documentation from plugins and modules included in the [Littlefork toolkit](https://gitlab.com/littlefork/littlefork-toolkit) and building a static using the [Metalsmith static site generator](http://www.metalsmith.io/) software.
